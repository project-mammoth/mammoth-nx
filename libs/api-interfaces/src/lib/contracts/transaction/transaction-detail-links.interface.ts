import { IFormattedNode } from '../generic';

export interface ITransactionDetailLinks {
  account: IFormattedNode;
  category: IFormattedNode;
  payee: IFormattedNode;
}
